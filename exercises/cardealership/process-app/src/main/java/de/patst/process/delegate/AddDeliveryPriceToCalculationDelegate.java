package de.patst.process.delegate;

import de.patst.process.MessageConstants;
import de.patst.process.model.CarContractDTO;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddDeliveryPriceToCalculationDelegate implements JavaDelegate {

  private static final Logger LOGGER = LoggerFactory.getLogger(AddDeliveryPriceToCalculationDelegate.class);

  @Autowired
  RuntimeService runtimeService;

  @Override
  public void execute(DelegateExecution execution) {
    CarContractDTO carContractDTO = (CarContractDTO) runtimeService.getVariable(execution.getId(),
            "carContract");
    //
    
    runtimeService
            .createMessageCorrelation(MessageConstants.START_CALCULATION_MESSAGE)
            .processInstanceBusinessKey(carContractDTO.getId())
            .setVariable("carContract", carContractDTO)
            .correlateStartMessage();

    LOGGER.info("Sending delivery price to calculation department for processInstanceId={} with businessKey={}",
        execution.getProcessInstanceId(), execution.getBusinessKey());
  }

}
