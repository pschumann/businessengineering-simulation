package de.patst.process.delegate;

import de.patst.process.MessageConstants;
import de.patst.process.model.CarContractDTO;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StartCarProductionDelegate implements JavaDelegate {

  private static final Logger LOGGER = LoggerFactory.getLogger(StartCarProductionDelegate.class);

  @Autowired
  RuntimeService runtimeService;

  @Override
  public void execute(DelegateExecution execution) {
    LOGGER.info("Sending order to start car production for processInstanceId={} with businessKey={}",
        execution.getProcessInstanceId(), execution.getBusinessKey());

    CarContractDTO carContractDTO = (CarContractDTO) runtimeService.getVariable(execution.getId(),
            "carContract");

    runtimeService
            .createMessageCorrelation(MessageConstants.START_CAR_PRODUCTION_MESSAGE)
            .processInstanceBusinessKey(carContractDTO.getId())
            .setVariable("carContract", carContractDTO)
            .correlateStartMessage();

  }
}
