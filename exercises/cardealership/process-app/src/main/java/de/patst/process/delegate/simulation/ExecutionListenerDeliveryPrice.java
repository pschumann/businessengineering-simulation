package de.patst.process.delegate.simulation;

import de.patst.process.MessageConstants;
import de.patst.process.model.CarContractDTO;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.Execution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ExecutionListenerDeliveryPrice implements JavaDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionListenerDeliveryPrice.class);


    @Autowired
    RuntimeService runtimeService;
    @Override
    public void execute(DelegateExecution execution) throws Exception {

        CarContractDTO carContractDTO = (CarContractDTO) runtimeService.getVariable(execution.getId(),
                "carContract");
        List<Execution> executions = runtimeService.createExecutionQuery()
                .signalEventSubscriptionName(MessageConstants.DELIVERY_PRICE_KNOWN_SIGNAL)
                .processInstanceBusinessKey(carContractDTO.getId())
                .list();

        Execution eventSubscription = executions.get(0);
        runtimeService
                .createSignalEvent(MessageConstants.DELIVERY_PRICE_KNOWN_SIGNAL)
                .executionId(eventSubscription.getId())
                .send();

        LOGGER.info("ExecutionListener: DeliveryPriceKnownSignal was send. ProcessInstanceId: {} with businessKey={}",
                execution.getProcessInstanceId(), carContractDTO.getId());
    }
}
