package de.patst.process.controller.simulation;

import de.patst.process.MessageConstants;
import de.patst.process.model.AddressDTO;
import de.patst.process.model.CarContractDTO;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;

@RestController
public class SimulationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimulationController.class);

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    Random random = new Random();

    Double lambda = 0.75;

    private double getRandomExponetialDouble(Double lambda) {
        return Math.log(1- random.nextDouble())/(-lambda);
    }

    private Double getRandomNormalDistributionDouble(Integer expectation, Integer standardDeviation){
        return this.random.nextGaussian() * standardDeviation + expectation;
    }

    @GetMapping("/simulate/{iterations}")
    public void simulateCarDealership(@PathVariable(name = "iterations") Integer iterations) {

        this.startThreads();

        this.simulateCustomer(iterations);

    }

    @GetMapping("/simulateCustomers/{iterations}")
    public void simulateCustomer(@PathVariable(name = "iterations") Integer iterations) {
        int i = 0;
        while(i < iterations) {
            i++;

            try {
                Long sleepTime = Math.round(this.getRandomExponetialDouble(lambda))*1000;
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                LOGGER.info(String.valueOf(e));
                Thread.currentThread().interrupt();
            }
            int finalI = i;
            Thread thread = new Thread(){
                @Override
                public void run(){

                    String businessKey = String.valueOf(finalI);
                    CarContractDTO carContract = new CarContractDTO();
                    carContract.setCarModel("VW-Polo");
                    carContract.setCustomerEmail("max.meier@gmx.net");
                    carContract.setId(businessKey);
                    carContract.setCustomerAddress(new AddressDTO("Freundallee", "15", "30173", "Hannover"));

                    runtimeService
                            .createMessageCorrelation(MessageConstants.CAR_CONTRACT_CREATED_MESSAGE)
                            .processInstanceBusinessKey(businessKey)
                            .setVariable("carContract", carContract)
                            .correlateStartMessage();

                    LOGGER.info("Correlated message for process start. BusinessKey: {}", businessKey);
                }
            };
            thread.start();
        }
    }

    @GetMapping("/startUserThreads")
    public void startThreads(){
        startWorkerThread(
                "SimulateCarCompletionDateDelegateId", 5000, 500);
        startWorkerThread(
                "SimulateDeliveryServiceDelegateId", 2000, 250);
        startWorkerThread(
                "SimulateCarInvoiceDelegateId", 500, 100);
    }

    private void startWorkerThread(String simulateCarCompletionDateDelegateId, int expectedValue, int standardDeviation) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                while (true) {
                    List<Task> taskList = taskService.createTaskQuery()
                            .taskDefinitionKey(simulateCarCompletionDateDelegateId)
                            .orderByTaskCreateTime()
                            .asc()
                            .list();

                    if (taskList.isEmpty()) {
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            LOGGER.info(String.valueOf(e));
                            Thread.currentThread().interrupt();
                        }
                    } else {
                        try {
                            Long sleepTime = Math.round(
                                    getRandomNormalDistributionDouble(expectedValue, standardDeviation));
                            Thread.sleep(sleepTime);
                        } catch (InterruptedException e) {
                            LOGGER.info(String.valueOf(e));
                            Thread.currentThread().interrupt();
                        }
                        Task task = taskList.get(0);
                        taskService.complete(task.getId());
                    }
                }

            }
        };
        thread.start();
    }
}
