package de.patst.process.delegate.simulation;

import de.patst.process.MessageConstants;
import de.patst.process.model.CarContractDTO;
import de.patst.process.model.CarInvoiceDTO;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SimulateCarInvoiceDelegate implements JavaDelegate {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimulateCarInvoiceDelegate.class);

    @Autowired
    RuntimeService runtimeService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        CarContractDTO carContractDTO = (CarContractDTO) runtimeService.getVariable(execution.getId(),
                "carContract");

        CarInvoiceDTO carInvoiceDTO = new CarInvoiceDTO(carContractDTO.getId(), 456d);

        runtimeService.createMessageCorrelation(MessageConstants.CAR_INVOICE_RECEIVED_MESSAGE)
                .setVariable("carInvoice", carInvoiceDTO)
                .processInstanceBusinessKey(carInvoiceDTO.getContractId()) // to get a unique process instance!
                .correlate();

        LOGGER.info("Created MessageCorrelation for CarInvoiceReceivedMessage: ProcessInstanceId: {} with businessKey={}",
                execution.getProcessInstanceId(), carContractDTO.getId());
    }
}
