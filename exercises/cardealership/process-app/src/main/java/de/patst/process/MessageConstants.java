package de.patst.process;

public class MessageConstants {

  public static final String CAR_CONTRACT_CREATED_MESSAGE = "CarContractCreatedMessage";

  public static final String CAR_COMPLETION_DATE_RECEIVED = "CarCompletionDateReceivedMessage";

  public static final String CAR_INVOICE_RECEIVED_MESSAGE = "CarInvoiceReceivedMessage";

  public static final String START_CAR_PRODUCTION_MESSAGE = "StartProductionMessage";

  public static final String START_DELIVERY_SERVICE_MESSAGE = "StartDeliveryServiceMessage";

  public static final String START_CALCULATION_MESSAGE = "StartCalculationMessage";

  public static final String DELIVERY_SERVICE_RESPONSE_RECEIVED_MESSAGE = "DeliveryServiceResponseReceivedMessage";

  public static final String DELIVERY_PRICE_KNOWN_SIGNAL = "DeliveryPriceKnownSignal";
}
