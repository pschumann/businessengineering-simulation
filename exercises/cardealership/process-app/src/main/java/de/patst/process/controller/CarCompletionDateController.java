package de.patst.process.controller;

import de.patst.process.MessageConstants;
import de.patst.process.model.CarCompletionDateDTO;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.MessageCorrelationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
public class CarCompletionDateController {

  private static final Logger LOGGER = LoggerFactory.getLogger(CarCompletionDateController.class);

  @Autowired
  private RuntimeService runtimeService;

  @PutMapping("/completion-date")
  public void correlateCarCompletionDate(
      @RequestBody CarCompletionDateDTO carCompletionDate) {
    // correlate a message
    MessageCorrelationResult correlationResult = runtimeService.createMessageCorrelation(MessageConstants.CAR_COMPLETION_DATE_RECEIVED)
        .setVariable("completionDate", carCompletionDate)
        .processInstanceBusinessKey(carCompletionDate.getContractId()) // to get a unique process instance!
        .correlateWithResult();
    Objects.requireNonNull(correlationResult);

    LOGGER.info("Correlated message for process instance {}", correlationResult.getExecution().getProcessInstanceId());
  }

}
