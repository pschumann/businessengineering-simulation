package de.patst.process.delegate.simulation;

import de.patst.process.MessageConstants;
import de.patst.process.model.CarContractDTO;
import de.patst.process.model.DeliveryResponseDTO;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class SimulateDeliveryServiceDelegate implements JavaDelegate {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimulateDeliveryServiceDelegate.class);

    @Autowired
    RuntimeService runtimeService;

    Random random = new Random();

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        CarContractDTO carContractDTO = (CarContractDTO) runtimeService.getVariable(execution.getId(),
                "carContract");

        String [] arr = {"accepted", "rejected"};

        // randomly selects an index from the arr
        int select = random.nextInt(arr.length);

        DeliveryResponseDTO deliveryResponseDTO = new DeliveryResponseDTO(carContractDTO.getId(),
                arr[select], 123d);

        runtimeService.createMessageCorrelation(MessageConstants.DELIVERY_SERVICE_RESPONSE_RECEIVED_MESSAGE)
                .setVariable("deliveryResponse",deliveryResponseDTO)
                .processInstanceBusinessKey(deliveryResponseDTO.getContractId())
                .correlate();

        LOGGER.info("Created MessageCorrelation for DeliveryServiceResponseReceivedMessage: ProcessInstanceId: {} " +
                        "with businessKey={}",
                execution.getProcessInstanceId(), carContractDTO.getId());
    }
}
