package de.patst.process.delegate.simulation;

import de.patst.process.MessageConstants;
import de.patst.process.model.CarCompletionDateDTO;
import de.patst.process.model.CarContractDTO;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class SimulateCarCompletionDateDelegate implements JavaDelegate {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimulateCarCompletionDateDelegate.class);

    @Autowired
    RuntimeService runtimeService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        CarContractDTO carContractDTO = (CarContractDTO) runtimeService.getVariable(execution.getId(),
                "carContract");

        CarCompletionDateDTO carCompletionDate = new CarCompletionDateDTO(LocalDateTime.now().plusMonths(1),
                carContractDTO.getId());

        runtimeService.createMessageCorrelation(MessageConstants.CAR_COMPLETION_DATE_RECEIVED)
                .setVariable("completionDate", carCompletionDate)
                .processInstanceBusinessKey(carCompletionDate.getContractId()) // to get a unique process instance!
                .correlate();

        LOGGER.info("Created MessageCorrelation for CarCompletionDateReceivedMessage: ProcessInstanceId: {} with businessKey={}",
                execution.getProcessInstanceId(), carContractDTO.getId());
    }
}
