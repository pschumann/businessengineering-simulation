package de.patst.process;

import de.patst.process.controller.simulation.SimulationController;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimulationTests {

    @Autowired
    SimulationController simulationController;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    HistoryService historyService;


    public static final int ITERATIONS = 10;
    /**
     * Throws:
     * Exception in thread "Thread-X" org.springframework.transaction.CannotCreateTransactionException:
     * Could not open JDBC Connection for transaction;
     * nested exception is org.h2.jdbc.JdbcSQLNonTransientConnectionException:
     * The database is open in exclusive mode; can not open additional connections [90135-200]
     *
     * This Problem occurs because this test starts Threads which are constantly looking for Tasks in the Database
     *  to execute
     */
    @Test
    public void testCompletionOfAllProcessIntances() throws InterruptedException {

        simulationController.simulateCarDealership(ITERATIONS);

        List<ProcessInstance> activeInstances = runtimeService.createProcessInstanceQuery()
                .active()
                .list();

        assert(!activeInstances.isEmpty());

        //Used to wait for the WorkerThreads
        Thread.sleep(120000);

        List<ProcessInstance> activeInstancesShouldBeEmpty = runtimeService.createProcessInstanceQuery()
                .active()
                .list();

        assertEquals(0, activeInstancesShouldBeEmpty.size());


    }
}
